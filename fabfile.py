from fabric.api import sudo, task


def install(package):
    """docstring for install"""
    sudo('apt-get -y install %s' % package)

@task
def install_lxc():
    """docstring for install_lxc"""

    packages = [
        'lxc',
        'bridge-utils',
        'libvirt-bin',
        'debootstrap'
    ]

    for package in packages:
        install(package)

def prepare_host():
    """docstring for prepare_host"""

    raise Exception("Dont use yet!")

    # TODO
    # add 
    # 'cgroup          /sys/fs/cgroup         cgroup  defaults        0       0'
    # to file /etc/fstab

    sudo('mount -a')
